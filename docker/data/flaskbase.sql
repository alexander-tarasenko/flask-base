--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.0
-- Dumped by pg_dump version 9.6.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: flaskbase
--

CREATE TABLE alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE alembic_version OWNER TO flaskbase;

--
-- Name: fragment; Type: TABLE; Schema: public; Owner: flaskbase
--

CREATE TABLE fragment (
    id integer NOT NULL,
    title character varying(100),
    start numeric(8,2) NOT NULL,
    "end" numeric(8,2) NOT NULL,
    moment_id integer
);


ALTER TABLE fragment OWNER TO flaskbase;

--
-- Name: fragment_id_seq; Type: SEQUENCE; Schema: public; Owner: flaskbase
--

CREATE SEQUENCE fragment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE fragment_id_seq OWNER TO flaskbase;

--
-- Name: fragment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: flaskbase
--

ALTER SEQUENCE fragment_id_seq OWNED BY fragment.id;


--
-- Name: moment; Type: TABLE; Schema: public; Owner: flaskbase
--

CREATE TABLE moment (
    id integer NOT NULL,
    title character varying(100),
    source_id character varying(100) NOT NULL,
    user_id integer
);


ALTER TABLE moment OWNER TO flaskbase;

--
-- Name: moment_id_seq; Type: SEQUENCE; Schema: public; Owner: flaskbase
--

CREATE SEQUENCE moment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE moment_id_seq OWNER TO flaskbase;

--
-- Name: moment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: flaskbase
--

ALTER SEQUENCE moment_id_seq OWNED BY moment.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: flaskbase
--

CREATE TABLE "user" (
    id integer NOT NULL,
    email character varying(320) NOT NULL,
    name character varying(100),
    avatar character varying(200),
    active boolean,
    tokens text,
    created_at timestamp without time zone
);


ALTER TABLE "user" OWNER TO flaskbase;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: flaskbase
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO flaskbase;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: flaskbase
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: fragment id; Type: DEFAULT; Schema: public; Owner: flaskbase
--

ALTER TABLE ONLY fragment ALTER COLUMN id SET DEFAULT nextval('fragment_id_seq'::regclass);


--
-- Name: moment id; Type: DEFAULT; Schema: public; Owner: flaskbase
--

ALTER TABLE ONLY moment ALTER COLUMN id SET DEFAULT nextval('moment_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: flaskbase
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: flaskbase
--

COPY alembic_version (version_num) FROM stdin;
8233e9d68538
\.


--
-- Data for Name: fragment; Type: TABLE DATA; Schema: public; Owner: flaskbase
--

COPY fragment (id, title, start, "end", moment_id) FROM stdin;
\.


--
-- Name: fragment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: flaskbase
--

SELECT pg_catalog.setval('fragment_id_seq', 1, false);


--
-- Data for Name: moment; Type: TABLE DATA; Schema: public; Owner: flaskbase
--

COPY moment (id, title, source_id, user_id) FROM stdin;
\.


--
-- Name: moment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: flaskbase
--

SELECT pg_catalog.setval('moment_id_seq', 1, false);


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: flaskbase
--

COPY "user" (id, email, name, avatar, active, tokens, created_at) FROM stdin;
\.


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: flaskbase
--

SELECT pg_catalog.setval('user_id_seq', 1, false);


--
-- Name: fragment fragment_pkey; Type: CONSTRAINT; Schema: public; Owner: flaskbase
--

ALTER TABLE ONLY fragment
    ADD CONSTRAINT fragment_pkey PRIMARY KEY (id);


--
-- Name: moment moment_pkey; Type: CONSTRAINT; Schema: public; Owner: flaskbase
--

ALTER TABLE ONLY moment
    ADD CONSTRAINT moment_pkey PRIMARY KEY (id);


--
-- Name: user user_email_key; Type: CONSTRAINT; Schema: public; Owner: flaskbase
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: flaskbase
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: fragment fragment_moment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: flaskbase
--

ALTER TABLE ONLY fragment
    ADD CONSTRAINT fragment_moment_id_fkey FOREIGN KEY (moment_id) REFERENCES moment(id);


--
-- Name: moment moment_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: flaskbase
--

ALTER TABLE ONLY moment
    ADD CONSTRAINT moment_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- PostgreSQL database dump complete
--

