var gulp  = require('gulp');
var touch = require('gulp-touch');
function touchPy() {
    console.log('checked changes')
    gulp.src('app/uwsgi/uwsgi.ini')
        .pipe(touch());
}

gulp.task('default', function() {

    // Changes to .html files don't trigger a runserver refresh w/o touching a .py file
    gulp.watch('./**/*.py').on('change', function(file) {
        // return gulp.log('Gulp is running!')
        touchPy();
    });
});