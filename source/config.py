# DATABASE SETTINGS
pg_db_username = 'flaskbase'
pg_db_password = 'flaskbase'
pg_db_name = 'flaskbase'
pg_db_hostname = 'db:5432'

DEBUG = True
# PORT = 5000
# HOST = "127.0.0.1"
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = True
SECRET_KEY = "SOME SECRET"


# PostgreSQL
SQLALCHEMY_DATABASE_URI = "postgresql://{DB_USER}:{DB_PASS}@{DB_ADDR}/{DB_NAME}".format(DB_USER=pg_db_username,
                                                                                        DB_PASS=pg_db_password,
                                                                                        DB_ADDR=pg_db_hostname,
                                                                                        DB_NAME=pg_db_name)
