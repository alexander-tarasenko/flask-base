class InvalidRequestObject(object):

    def __init__(self):
        self.errors = []

    def add_error(self, parameter, message):
        self.errors.append({'parameter': parameter, 'message': message})

    def has_errors(self):
        return len(self.errors) > 0

    def __nonzero__(self):
        return False

    __bool__ = __nonzero__


class ValidRequestObject(object):

    errors = []

    def __init__(self, dictionary):
        self.__dict__.update(dictionary)
        # self.filters = filters

    def __getattr__(self, item):
        return None

    @property
    def is_valid(self):
        return not len(self.errors)

    @classmethod
    def from_dict(cls, adict):
        raise NotImplementedError

    def __nonzero__(self):
        return True

    __bool__ = __nonzero__