import * as React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Router, IndexRoute, Route } from 'react-router'
import { UsersCntr, LoginCntr, LayoutCntr } from './containers'
import { Home, Admin, Users } from './components'

import { store, history } from './store'

render(// https://github.com/ReactTraining/react-router/blob/master/docs/API.md
    <Provider store={ store }>
        <Router history={ history }>
            <Route path="/" component={ LayoutCntr }>
                <Route onEnter={ checkAuth }>
                    <Route path="admin" component={ Admin }/>
                </Route>
                <IndexRoute component={ Home } />
                <Route path="users" component={ UsersCntr }/>
                <Route path="users/:id" component={ UsersCntr }/>
                <Route path="login" component={ LoginCntr }/>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('app')
)

function checkAuth(history, replace) {
    let { currentUser: { role } } = store.getState() as any

    if (role !== 'Admin') {
        replace('login')
    }
}
