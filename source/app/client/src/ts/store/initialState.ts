export const INITIAL_STATE = {
    currentUser: {
        
    },
    users: [{
        name: 'John',
        role: 'Admin',
        id: 'YXNmYWY='
    },{
        name: 'Jill',
        role: 'User',
        id: 'dHJqanI='
    },{
        name: 'Jamie',
        role: 'User',
        id: 'dWtqa3k='
    }]
}
