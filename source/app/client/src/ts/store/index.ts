import { createStore, applyMiddleware, compose } from 'redux'
import { syncHistoryWithStore } from 'react-router-redux'
import { hashHistory } from 'react-router'
import thunk from 'redux-thunk'
import { INITIAL_STATE } from './initialState'
import { rootReducer } from '../reducers'


interface IWindow extends Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
}

// interface INodeModule extends NodeModule {
//     onReload: any
// }

declare const window: IWindow
// declare const module: INodeModule

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = createStore(
    rootReducer,
    INITIAL_STATE,
    composeEnhancers(
        applyMiddleware(thunk)
    )
)

export const history = syncHistoryWithStore(hashHistory, store)

// if (module.onReload) {
//     module.onReload(() => {
//         const nextReducer = require('../reducers')
//         store.replaceReducer(nextReducer.default || nextReducer)
//         return true
//     })
// }
