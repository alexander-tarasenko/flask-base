export function makeActionCreator(type, ...argNames) {
    return function(...args) {
        let action = { type }

        argNames.forEach((arg, idx) =>
            action[argNames[idx]] = args[idx]
        )

        return action
    }
}
