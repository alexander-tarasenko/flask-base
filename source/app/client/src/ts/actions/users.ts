import { makeActionCreator } from './makeActionCreator'

export const addUser = makeActionCreator('ADD_USER', 'user')
export const removeUser = makeActionCreator('REMOVE_USER', 'id')
