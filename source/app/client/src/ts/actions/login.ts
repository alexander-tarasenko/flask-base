import request from 'request'
import Promise from 'bluebird'

import { makeActionCreator } from './makeActionCreator'

Promise.promisifyAll(request)

export const logout = makeActionCreator('LOGOUT')

export function login(credentials) {
    return dispatch => {
        dispatch({
            type: 'LOGIN_REQUEST'
        })

        request
            .postAsync('http://127.0.0.1:3030/login', { json: credentials })
            .then(result =>
                dispatch({
                    type: 'LOGIN_RESPONSE',
                    data: result.body
                })
            )
            .catch(err =>
                dispatch({
                    type: 'LOGIN_ERROR',
                    err
                })
            )
    }
}
