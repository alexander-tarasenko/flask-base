import { connect } from 'react-redux'
import { Users, Login, Layout } from '../components'
import { bindActionCreators } from 'redux'
import * as actions from '../actions'

const actionsList = { ...actions }

export const LayoutCntr = connectStoreWithReactComponent(Layout)
export const UsersCntr = connectStoreWithReactComponent(Users)
export const LoginCntr = connectStoreWithReactComponent(Login)

function connectStoreWithReactComponent(Cmp) {
    return connect(
        state => state,
        dispatch => bindActionCreators(actionsList, dispatch)
    )(Cmp)
}
