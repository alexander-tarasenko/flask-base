import { INITIAL_STATE } from '../store/initialState'

export function currentUser(state = INITIAL_STATE.currentUser, action: any) {
    switch(action.type) {
        case 'LOGIN_RESPONSE':
            return action.data

        case 'LOGOUT':
            return {}

        default:
            return state
    }
}
