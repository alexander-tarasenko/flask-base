import { INITIAL_STATE } from '../store/initialState'

export function users(state = INITIAL_STATE.users, action: any) {
    switch(action.type) {
        case 'ADD_USER': {
            let { user } = action
            return state.concat(user)
        }

        case 'REMOVE_USER': {
            let { id } = action
            return state.filter(user => user.id !== id)
        }

        default:
            return state
    }
}
