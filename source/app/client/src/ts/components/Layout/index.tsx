import * as React from 'react'
import { Menu, Segment, Container, Dropdown, Button } from 'semantic-ui-react'
import { Link } from 'react-router'

export class Layout extends React.Component<any, {}> {
    render() {
        const { location: { pathname }, router, logout, currentUser: { role }} = this.props

        return (
            <div>
                <Segment inverted>
                    <Menu inverted pointing secondary>
                        <Menu.Item active={ pathname === '/' }>
                            <Link to="/">Home</Link>
                        </Menu.Item>
                        <Menu.Item active={ /^\/users\/?.*/.test(pathname) }>
                            <Link to="/users">Users</Link>
                        </Menu.Item>
                        <Menu.Item active={ pathname === '/admin' }>
                            <Link to="/admin">Admin</Link>
                        </Menu.Item>

                        <Menu.Menu position='right'>
                          <Menu.Item>
                            { role == null
                                ?   <Button primary
                                        onClick={ () => router.push('login') }
                                    >
                                        Sign In
                                    </Button>
                                :   <Button primary
                                        onClick={ () => {
                                            logout()
                                            router.push('')
                                        }}
                                    >
                                        Sign Out
                                    </Button>
                            }

                          </Menu.Item>
                        </Menu.Menu>
                    </Menu>
                </Segment>
                <Container>
                    { this.props.children }
                </Container>
            </div>
        )
    }
}
