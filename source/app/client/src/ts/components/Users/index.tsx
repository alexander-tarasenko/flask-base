import * as React from 'react'
import { Table, Button } from 'semantic-ui-react'

export class Users extends React.Component<any, {}> {
    render() {
        let { users, router } = this.props

        return (
            <Table basic>
             <Table.Header>
               <Table.Row>
                 <Table.HeaderCell>Name</Table.HeaderCell>
                 <Table.HeaderCell>Role</Table.HeaderCell>
                 <Table.HeaderCell textAlign="center">Actions</Table.HeaderCell>
               </Table.Row>
             </Table.Header>

             <Table.Body>
             {
                 users.map(({ name, role, id }) => (
                     <Table.Row key={ id }>
                       <Table.Cell>{ name }</Table.Cell>
                       <Table.Cell>{ role }</Table.Cell>
                       <Table.Cell collapsing>
                          <Button size="mini"
                             onClick={ () => router.push('users/' + id) }
                          >Show</Button>
                       </Table.Cell>
                     </Table.Row>
                 ))
             }
             </Table.Body>
           </Table>
        )
    }
}
