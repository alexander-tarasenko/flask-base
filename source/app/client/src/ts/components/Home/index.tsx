import * as React from 'react'
import { Message } from 'semantic-ui-react'

export class Home extends React.Component<any, {}> {
    state = {
        messageVisible: true
    }

    render() {
        const { messageVisible } = this.state

        return (
            <div>
                { messageVisible &&
                    <Message
                      onDismiss={ () => this.setState({ messageVisible: false }) }
                      header='Welcome!'
                      content='This is a special notification which you can dismiss.'
                    />
                }

            </div>
        )
    }
}
