import * as React from 'react'
import { Form, Checkbox, Button } from 'semantic-ui-react'

export class Login extends React.Component<any, {}> {
    render() {
        let username, password

        return (
            <Form>
                <Form.Field>
                  <label>Username</label>
                  <input ref={ el => username = el } type="text" />
                </Form.Field>
                <Form.Field>
                  <label>Password</label>
                  <input ref={ el => password = el } type="password" />
                </Form.Field>
                <Form.Field>
                  <Checkbox label='Remember Me' />
                </Form.Field>
                <Button onClick={ e => this.login(e, username.value, password.value) }>Login</Button>
          </Form>
        )
    }

    login(e, username, password) {
        let { login } = this.props

        login({
            username,
            password
        })

        e.preventDefault()
    }
}
