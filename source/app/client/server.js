import express from 'express'
import bodyParser from 'body-parser'

let app = express()

app.use(express.static(__dirname))
app.use(bodyParser.json())

app.post('/login', (req, res) => {
    let { username, password } = req.body

    console.log(username, password)

    if (username === 'admin' && password === '123') {
        res.json({
            name: 'Jasper',
            id: 'dHlqNGQ=',
            role: 'Admin'
        })
        return 
    }

    res.json({
        name: username,
        id: 'aHlqBGQ=',
        role: 'User'
    })
})

app.listen(3030, () =>
    console.log('Server started!')
)
