import gulp from 'gulp'
import gutil from 'gulp-util'
import browserify from 'browserify'
import tsify from 'tsify'
import babelify from 'babelify'
import source from 'vinyl-source-stream'
import rimraf from 'rimraf'
import rename from 'gulp-rename'

import postCSS from 'gulp-postcss'
import preCSS from 'precss'
import cssnano from 'cssnano'
import comments from 'postcss-inline-comment'

import livereload from 'gulp-livereload'
import browserifyInc from 'browserify-incremental'
import lrload from 'livereactload'
import express from 'express'

const isDev = true//process.env.NODE_ENV != 'production'

browserifyInc.args.debug = isDev

const
    bundler = browserify(Object.assign(browserifyInc.args, {
        entries: ['src/ts/bootstrap.tsx'],
        // plugin: isDev ? [lrload] : []
    }))

browserifyInc(bundler, {
    cacheFile: './browserify-cache.json'
})

bundler.plugin(tsify)

bundler.transform(
    babelify.configure({
        presets: ['es2015', 'react'],
        extensions: ['.ts', '.tsx']
    })
)

if (!isDev) {
    bundler.transform({
        global: true
    }, 'uglifyify')
}


export function bundle() {
    gutil.log('Compiling JS...');

    return bundler.bundle()
        .on('error', function (err) {
            gutil.log(err.message);
            this.emit("end");
        })
        .pipe(source('js/main.js'))
        .pipe(gulp.dest('./dist'))
        .pipe(livereload())
}

export function postcss() {
    return gulp.src(['./src/scss/styles.scss'])
        // .pipe(mixins())
        .pipe(postCSS([
            preCSS({ }),
            // mixins(),
            comments(),
            cssnano
        ]))
        .pipe(rename('styles.css'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(livereload())
}

export function images() {
    return gulp.src(['./src/images/*'])
        .pipe(gulp.dest('./dist/images'))
}

export function build(cb) {
    rimraf('./dist', () =>
        gulp.parallel(postcss, bundle)(cb)
    )
}

export function watch() {
    livereload.listen()
    build()
    // serve()
    gulp.watch(['./src/scss/**/*.scss'], postcss)
    gulp.watch(['./src/ts/**/*{ts,tsx}'], bundle)
}

export function serve() {
    let app = express()

    app.use(express.static(__dirname))

    app.listen(3030, () =>
        gutil.log('Server started!')
    )
}
