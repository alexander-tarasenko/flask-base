from models import Fragment

import graphene
from graphene import Node
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField


class FragmentInput(graphene.InputObjectType):
    start = graphene.String()
    end = graphene.String()
    title = graphene.String()


class FragmentNode(SQLAlchemyObjectType):
    class Meta:
        model = Fragment

        interfaces = (Node,)


class FragmentQuery(graphene.AbstractType):
    fragments = SQLAlchemyConnectionField(FragmentNode)
