import graphene
from graphene import relay
from .fragment import FragmentQuery
from .moment import MomentQuery, MomentMutation
from .user import UserMutation, UserQuery
from .youtube import YoutubeQuery


class RootQuery(graphene.ObjectType, FragmentQuery, MomentQuery, UserQuery, YoutubeQuery):
    node = relay.Node.Field()
    pass


class RootMutation(graphene.ObjectType, MomentMutation, UserMutation):
    pass


schema = graphene.Schema(query=RootQuery, mutation=RootMutation)