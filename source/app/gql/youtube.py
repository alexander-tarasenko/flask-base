import graphene
from graphene import Node, ObjectType
from models import Moment
from use_cases.request_objects import MomentMetaRequestObject
from use_cases.moment import MetadataUseCase
from collections import namedtuple


class DictNode(Node):
    @staticmethod
    def to_global_id(type, id):
        return id

    @staticmethod
    def get_node_from_global_id(global_id, context, info):
        request = MomentMetaRequestObject.prepare({'id': global_id})
        data = MetadataUseCase(Moment).execute(request)
        return namedtuple('NodeData', data.keys())(*data.values())


class StatisticType(ObjectType):
    favoriteCount = graphene.Int()
    commentCount = graphene.Int()
    viewCount = graphene.Int()
    likeCount = graphene.Int()
    dislikeCount = graphene.Int()

    @staticmethod
    def resolver(root, args, context, info):
        data = root.statistics
        return namedtuple('NodeData', data.keys())(*data.values())


class LocalizedType(ObjectType):
    title = graphene.String()
    description = graphene.String()

    @staticmethod
    def resolver(root, args, context, info):
        data = root.snippet.get('localized')
        return namedtuple('NodeData', data.keys())(*data.values())


class ThumbnailType(ObjectType):
    url = graphene.String()
    width = graphene.Int()
    height = graphene.Int()

    @staticmethod
    def resolver(source):
        def resolver(root, args, context, info):
            data = getattr(root, source)
            return namedtuple('NodeData', data.keys())(*data.values())
        return resolver


class ThumbnailsType(ObjectType):
    default = graphene.Field(ThumbnailType, resolver=ThumbnailType.resolver('default'))
    medium = graphene.Field(ThumbnailType, resolver=ThumbnailType.resolver('medium'))
    high = graphene.Field(ThumbnailType, resolver=ThumbnailType.resolver('high'))
    standard = graphene.Field(ThumbnailType, resolver=ThumbnailType.resolver('standard'))
    maxres = graphene.Field(ThumbnailType, resolver=ThumbnailType.resolver('maxres'))

    @staticmethod
    def resolver(root, args, context, info):

        data = root.snippet.get('thumbnails')
        return namedtuple('NodeData', data.keys())(*data.values())


class MetaNode(DictNode):
    id = graphene.String()
    kind = graphene.String()
    etag = graphene.String()
    title = graphene.String()
    description = graphene.String()
    localized = graphene.Field(LocalizedType, resolver=LocalizedType.resolver)
    thumbnails = graphene.Field(ThumbnailsType, resolver=ThumbnailsType.resolver)
    statistics = graphene.Field(StatisticType, resolver=StatisticType.resolver)


class MomentMetaType(ObjectType):

    class Meta:
        interfaces = (MetaNode,)


class YoutubeQuery(graphene.AbstractType):
    meta = MetaNode.Field(MomentMetaType)