
import graphene
from graphene import Node
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from models import Moment
from .fragment import FragmentInput
from use_cases.request_objects import AddMomentRequestObject, UpdateMomentRequestObject
from use_cases.moment import AddMomentUseCase, UpdateMomentUseCase


class MomentType(SQLAlchemyObjectType):
    class Meta:
        model = Moment

        interfaces = (Node,)


class CreateMoment(graphene.Mutation):
    class Input:
        source_id = graphene.String()
        title = graphene.String()
        user_id = graphene.String()
        fragments = graphene.List(FragmentInput)

    moment = graphene.Field(lambda: MomentType)

    def mutate(self, args, context, info):
        request = AddMomentRequestObject.prepare(args)
        if request.is_valid:
            return CreateMoment(moment=AddMomentUseCase(Moment).execute(request))

        return request.errors


class UpdateMoment(graphene.Mutation):
    class Input:
        id = graphene.ID()
        title = graphene.String()
        source_id = graphene.String()
        user_id = graphene.String()
        fragments = graphene.List(FragmentInput)

    moment = graphene.Field(lambda: MomentType)

    def mutate(self, args, context, info):

        request = UpdateMomentRequestObject.prepare(args)
        if request.is_valid:
            return UpdateMoment(moment=UpdateMomentUseCase(Moment).execute(request))

        return request.errors


class MomentMutation(graphene.AbstractType):
    create_moment = CreateMoment.Field()
    update_moment = UpdateMoment.Field()


class MomentQuery(graphene.AbstractType):
    moment = Node.Field(MomentType)
    moments = SQLAlchemyConnectionField(MomentType)