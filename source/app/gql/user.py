import graphene
from graphene import Node
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from models import User
from use_cases.request_objects import AddUserRequestObject
from use_cases.user import AddUserUseCase


class UserNode(SQLAlchemyObjectType):
    class Meta:
        model = User
        interfaces = (Node,)


class CreateUser(graphene.Mutation):
    class Input:
        name = graphene.String()
        email = graphene.String()

    user = graphene.Field(lambda: UserNode)

    def mutate(self, args, context, info):
        print('prepare ' * 100)
        request = AddUserRequestObject.prepare(args)
        print('request ' * 100)
        print(request)
        if request.is_valid:
            return CreateUser(user=AddUserUseCase(User).execute(request))

        return request.errors


class UserMutation(graphene.AbstractType):
    create_user = CreateUser.Field()


class UserQuery(graphene.AbstractType):
    user = Node.Field(UserNode)
    users = SQLAlchemyConnectionField(UserNode)





