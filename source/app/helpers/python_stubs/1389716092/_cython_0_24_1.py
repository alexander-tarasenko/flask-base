# encoding: utf-8
# module _cython_0_24_1
# from /usr/local/lib/python3.6/site-packages/Cython/Tempita/_tempita.cpython-36m-x86_64-linux-gnu.so
# by generator 1.144
# no doc
# no imports

# Variables with simple values

__loader__ = None

__spec__ = None

# no functions
# classes

class cython_function_or_method(object):
    def __call__(self, *args, **kwargs): # real signature unknown
        """ Call self as a function. """
        pass

    def __get__(self, *args, **kwargs): # real signature unknown
        """ Return an attribute of instance, which is of type owner. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    func_closure = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    func_code = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    func_defaults = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    func_dict = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    func_doc = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    func_globals = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    func_name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    __annotations__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    __closure__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    __code__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    __defaults__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    __globals__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    __kwdefaults__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    __self__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __dict__ = None # (!) real value is ''
    __name__ = 'cython_function_or_method'
    __qualname__ = 'cython_function_or_method'


