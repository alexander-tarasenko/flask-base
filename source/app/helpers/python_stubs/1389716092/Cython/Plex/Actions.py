# encoding: utf-8
# module Cython.Plex.Actions
# from /usr/local/lib/python3.6/site-packages/Cython/Plex/Actions.cpython-36m-x86_64-linux-gnu.so
# by generator 1.144
# no doc

# imports
import builtins as __builtins__ # <module 'builtins' (built-in)>

# no functions
# classes

class Action(object):
    # no doc
    def same_as(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    __pyx_vtable__ = None # (!) real value is ''


class Begin(Action):
    """
    Begin(state_name) is a Plex action which causes the Scanner to
        enter the state |state_name|. See the docstring of Plex.Lexicon
        for more information.
    """
    def same_as(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, state_name): # real signature unknown; restored from __doc__
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    __pyx_vtable__ = None # (!) real value is ''


class Call(Action):
    """ Internal Plex action which causes a function to be called. """
    def same_as(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    __pyx_vtable__ = None # (!) real value is ''


class Ignore(Action):
    """
    IGNORE is a Plex action which causes its associated token
        to be ignored. See the docstring of Plex.Lexicon  for more
        information.
    """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    __pyx_vtable__ = None # (!) real value is ''


class Return(Action):
    """
    Internal Plex action which causes |value| to
        be returned as the value of the associated token
    """
    def same_as(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    __pyx_vtable__ = None # (!) real value is ''


class Text(Action):
    """
    TEXT is a Plex action which causes the text of a token to
        be returned as the value of the token. See the docstring of
        Plex.Lexicon  for more information.
    """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    __pyx_vtable__ = None # (!) real value is ''


# variables with complex values

IGNORE = None # (!) real value is ''

TEXT = None # (!) real value is ''

__loader__ = None # (!) real value is ''

__spec__ = None # (!) real value is ''

__test__ = {}

