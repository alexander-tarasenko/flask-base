# encoding: utf-8
# module Cython.Runtime.refnanny
# from /usr/local/lib/python3.6/site-packages/Cython/Runtime/refnanny.cpython-36m-x86_64-linux-gnu.so
# by generator 1.144
# no doc

# imports
import builtins as __builtins__ # <module 'builtins' (built-in)>

# Variables with simple values

loglevel = 0

LOG_ALL = 1
LOG_NONE = 0

RefNannyAPI = 139998972362336

# no functions
# classes

class Context(object):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    errors = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    filename = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    refs = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    start = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __pyx_vtable__ = None # (!) real value is ''


# variables with complex values

reflog = []

__loader__ = None # (!) real value is ''

__spec__ = None # (!) real value is ''

__test__ = {}

