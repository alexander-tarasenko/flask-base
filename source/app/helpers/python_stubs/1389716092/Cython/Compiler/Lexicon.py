# encoding: utf-8
# module Cython.Compiler.Lexicon
# from /usr/local/lib/python3.6/site-packages/Cython/Compiler/Lexicon.cpython-36m-x86_64-linux-gnu.so
# by generator 1.144
# no doc

# imports
import builtins as __builtins__ # <module 'builtins' (built-in)>

# Variables with simple values

any_string_prefix = 'rRfFuUbBcC'

bytes_prefixes = 'bB'

char_prefixes = 'cC'

IDENT = 'IDENT'

raw_prefixes = 'rR'

string_prefixes = 'fFuUbB'

# functions

def make_lexicon(*args, **kwargs): # real signature unknown
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is ''

__spec__ = None # (!) real value is ''

__test__ = {}

