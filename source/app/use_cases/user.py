from shared import use_case as uc
from shared import response_object as res
from models import User
from main import db

class AddUserUseCase(uc.UseCase):

    def __init__(self, repo):
        self.repo = repo

    def process_request(self, request):
        user = User(name=request.name, email=request.email)
        db.session.add(user)
        db.session.commit()
        return user
