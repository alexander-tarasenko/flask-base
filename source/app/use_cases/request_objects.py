import collections

from shared import request_object as req


class AddUserRequestObject(req.ValidRequestObject):

    @classmethod
    def prepare(cls, adict):
        # @TODO validate request

        return AddUserRequestObject(adict)


class MomentMetaRequestObject(req.ValidRequestObject):

    @classmethod
    def prepare(cls, adict):
        # @TODO validate request

        return MomentMetaRequestObject(adict)


class AddMomentRequestObject(req.ValidRequestObject):

    @classmethod
    def prepare(cls, adict):
        # @TODO validate request

        return AddMomentRequestObject(adict)


class UpdateMomentRequestObject(req.ValidRequestObject):

    @classmethod
    def prepare(cls, adict):
        # @TODO validate request

        return UpdateMomentRequestObject(adict)
