from shared import use_case as uc
from shared import response_object as res
from models import Moment, Fragment
from graphene import Node
from main import db, app

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from oauth2client.tools import argparser


class MetadataUseCase(uc.UseCase):

    def __init__(self, repo):
        self.repo = repo

    def process_request(self, request):
        service_name = app.config.get('YOUTUBE_API_SERVICE_NAME')
        api_version = app.config.get('YOUTUBE_API_VERSION')
        api_key = app.config.get('YOUTUBE_API_KEY')
        youtube = build(service_name, api_version, developerKey=api_key)

        # Call the videos.list method to retrieve location details for each video.
        video = youtube.videos().list(
            id=request.id,
            part='snippet,statistics'
        ).execute()

        return video.get('items')[0]


class AddMomentUseCase(uc.UseCase):

    def __init__(self, repo):
        self.repo = repo

    def process_request(self, request):
        moment = Moment(source_id=request.source_id, title=request.title, user_id=request.user_id)
        moment.fragments = [Fragment(**data) for data in request.fragments]
        db.session.add(moment)
        db.session.commit()
        return moment


class UpdateMomentUseCase(uc.UseCase):

    def __init__(self, repo):
        self.repo = repo

    def process_request(self, request):
        moment = Moment.query.get(Node.from_global_id(request.id)[1])
        moment.fragments = [Fragment(**data) for data in request.fragments]
        moment.source_id = request.source_id
        moment.title = request.title
        moment.user_id = request.user_id
        db.session.commit()
        return moment
