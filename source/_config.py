import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Auth:
    CLIENT_ID = ('467811958251-eqahg5rvg1rtj8v5ptjlbd8brfs2rt1p.apps.googleusercontent.com')
    CLIENT_SECRET = 'xgevYkYyhUVhA4awDHCd2M0B'
    REDIRECT_URI = 'http://dashboard-local.com:8080/gCallback'
    AUTH_URI = 'https://accounts.google.com/o/oauth2/auth'
    TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'
    USER_INFO = 'https://www.googleapis.com/userinfo/v2/me'
    SCOPE = ['profile', 'email']


class Config(object):
    DEBUG = True
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    STATIC_URL_PATH = ''
    STATIC_FOLDER = 'client/dist'
    TEMPLATE_FOLDER = 'client'
    OAUTHLIB_INSECURE_TRANSPORT = '1'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    YOUTUBE_API_SERVICE_NAME = 'youtube'
    YOUTUBE_API_VERSION = 'v3'
    YOUTUBE_API_KEY = 'AIzaSyAEqnT4_hz1-NLDQ0gyFSq-vPG-I7o3Z5I'


class ProductionConfig(Config):
    DEBUG = False
    SECRET_KEY = 'flaskbase-prod'


class StagingConfig(Config):
    DEVELOPMENT = True
    SECRET_KEY = 'flaskbase-stag'
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    SECRET_KEY = 'flaskbase-dev'
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
